/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectomodelo;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author jacka
 */
@ManagedBean
@RequestScoped
public class Aspirante {

     private final static char SEXO_DEF ='H';
    private final static String[] PROFESION_DEF ={"ABOGADO", "INGENIERO EN SISTEMAS","ADMINISTRACION DE EMPRESAS", "PSICOLOGIA"};
    private final static int NUM_MAX_ASPIRANTES = 5;
    
    private int id;
    private String nombre;
    private String apellido;
    private int edad;
    private int cedula;
    private char sexo;
    private String profesion;
    
    
    
    public String getNombre() {
        return nombre;
    }

    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }
    
     

    
    public String getApellido() {
        return apellido;
    }

    
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }


    public int getCedula() {
        return cedula;
    }
   
    public void setCedula(int cedula) {
        this.cedula = cedula;
    }
   
    public int getEdad() {
        return edad;
    }
    
    public void setEdad(int edad) {
        this.edad = edad;
    }
            

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
    
    private void comprobarSexo(){
    if (sexo !='H' && sexo!='M'){
        this.sexo = SEXO_DEF;
    }
    
}
    

    
    public String getProfesion() {
        return profesion;
    }

    
    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }
    
    public String enviar(){
        return "resultado";
    }

    public Aspirante(String nombre, int cedula,int edad,char sexo, String profesion) {
        this.nombre =nombre;
        this.cedula= cedula;
        this.profesion=profesion;
        this.sexo = sexo;
        comprobarSexo();
    }
    public Aspirante() {
    }

    public void setId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
