/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.controlador;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import proyectomodelo.Aspirante;
import proyectoservicio.ServicioAspirante;
/**
 *
 * @author jacka
 */
@ManagedBean
@RequestScoped
@ViewScoped
public class AcualizarAspirante implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private ServicioAspirante serviciosAspirante;
    private List<String> profesion;
    private String profesionSeleccionada;
    private Aspirante aspirante;
    
    
    
    
    @PostConstruct
    public void init(){
        FacesContext fc = FacesContext.getCurrentInstance();
        String userId= fc.getExternalContext().getRequestParameterMap().get("userId");
        aspirante = serviciosAspirante.obtenerObjetoPorId(userId);
    }
    public void actualizar(){
        aspirante.setProfesion(profesionSeleccionada);
        serviciosAspirante.actualizar(aspirante);
    }

    public List<String> getProfesion() {
        return profesion;
    }

    public void setProfesion(List<String> profesion) {
        this.profesion = profesion;
    }

    public String getProfesionSeleccionada() {
        return profesionSeleccionada;
    }

    public void setProfesionSeleccionada(String profesionSeleccionada) {
        this.profesionSeleccionada = profesionSeleccionada;
    }

    public Aspirante getAspirante() {
        return aspirante;
    }

    public void setAspirante(Aspirante aspirante) {
        this.aspirante = aspirante;
    }
    
    
    
}
