/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.controlador;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import proyectomodelo.Aspirante;
import proyectoservicio.ServicioAspirante;

/**
 *
 * @author jacka
 */
@ManagedBean
@RequestScoped
public class CrearAspirante implements Serializable{

  private static final long serialVersionUID=1L;
  
  private Aspirante aspirante;
  private ServicioAspirante servicioAspirante;
  private List<String> profesion;
  private String profesionSeleccionada;

    public String getProfesionSeleccionada() {
        return profesionSeleccionada;
    }

    public void setProfesionSeleccionada(String profesionSeleccionada) {
        this.profesionSeleccionada = profesionSeleccionada;
    }
  public void init(){
      profesion = servicioAspirante.profesion();
      aspirante = new Aspirante();
  }
  public void crear() throws IOException{
      servicioAspirante.create(aspirante);
      ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
      context.redirect("index.xhtml");
  }

    public Aspirante getAspirante() {
        return aspirante;
    }

    public void setAspirante(Aspirante aspirante) {
        this.aspirante = aspirante;
    }

    public List<String> getProfesion() {
        return profesion;
    }

    public void setProfesion(List<String> profesion) {
        this.profesion = profesion;
    }
    
    
}
