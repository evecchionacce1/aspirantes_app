/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoservicio;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import proyectomodelo.Aspirante;
/**
 *
 * @author jacka
 */
@ManagedBean
@RequestScoped

public class ServicioAspirante {

    private List<Aspirante> aspirantes = new ArrayList<Aspirante>();
   
    public void create(Aspirante aspirante){
        aspirante.setId(aspirantes.size()+1);
        aspirantes.add(aspirante);
        
    }
    public void actualizar(Aspirante aspirante){
        aspirante.setNombre(aspirante.getNombre());
        aspirante.setApellido(aspirante.getApellido());
        aspirante.setEdad(aspirante.getEdad());
        aspirante.setCedula(aspirante.getCedula());
        aspirante.setSexo(aspirante.getSexo());
        aspirante.setProfesion(aspirante.getProfesion());
        
    }
    public List<Aspirante> lista(){
        return aspirantes;
    }
    public List<String> profesion(){
        List<String> profesion = new ArrayList<String>();
        profesion.add("ABOGADO");
        profesion.add("INGENIERO EN SISTEMAS");
        profesion.add("ADMINISTRACION DE EMPRESAS");
        profesion.add("PSICOLOGIA");
        return profesion;
    }
    
    public Aspirante obtenerObjetoPorId(String userId){
        return aspirantes.get(Integer.parseInt(userId)-1);
    }
    public List<Aspirante> getAspirante(){
        return aspirantes;
    }
    public void setAspirante(List<Aspirante> aspirantes){
        this.aspirantes= aspirantes;
    }
}
