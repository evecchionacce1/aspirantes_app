## Proyecto: Aspirantes

Este proyecto es la se realizó con la finalidad de crear un aplicativo web utiliazndo conocimientos en Java y Mysql para validar los conocimientos de Edanir Vecchionacce en temas de automatizacion.

### Este proyecto cuenta con las siguientes características:

Lenguaje de programación: Java 
Framework: Java Server Faces
Patrón de diseño: Screenplay
Motor de BD: MySQL


### Estructura del proyecto:

**carpeta Aspirantes_App** esta carpeta contiene el proyecto de programacion.

**WebPages/WebInf/:** Esta carpeta contiene las vistas web del aplicativo.

**SourcePackages/conexion//:** Este paquete contiene las conexiones a base de datos.

**SourcePackages/consultas//:** Este paquete maneja las funciones de iteraciones con elementos web, por ejemplo, consultar los datos registrado.

**SourcePackages/proyecto.controlador//:** Este paquete maneja todas las funciones de iteraciones con elementos web, por ejemplo, crear y actualizar registros.

**SourcePackages/proyectomodelo//:** Este paquete maneja todas las funciones del obeto aspirante.

**SourcePackages/proyectoservicio//:** Este paquete maneja algunas de las funciones de iteraciones que pueden realizar los objetos.



### Pasos para obtener el proyecto de Java JSF:

<ul>
<li>Clonar el repositorio en la ubicación deseada, usando el comando: git clone https://gitlab.com/evecchionacce1/travelocityprueba.git
</ul>

### Pasos para ejecutar el proyecto

Se debe primero realizar los pasos de la sección "Pasos para obtener el proyecto de programacion"
<ul>
<li>Opción 1: Abrir el proyecto en el IDE netbeans, ejecutar el proyecto, asegurarse de tener ApacheTomcat para el servidor.



